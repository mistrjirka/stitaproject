import Vue, {VueConstructor} from "vue";
import Vuex from "vuex";
import { IAd } from "../Definitionsfornetwork";
import { ICachedAds } from "../Definitions";
import { UserApi } from "../NetworkManager";
import Loggedin from "../views/Loggedin.vue";
import SignPost from "../components/signpost.vue";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: "",
    url: "http://localhost:5000/",
    ads: {} as ICachedAds,
    dataReady: false,
    defaultview: SignPost as VueConstructor,
    secondaryview: Loggedin as VueConstructor
  },
  mutations: {
    setToken(state, payload) {
      state.token = payload;
    },
    addAd(state, payload: IAd ) {
      state.ads[payload.id] = payload;
    },
  },
  actions: {
    TodoById: async ({state}, id: number) => {
      if (state.ads[id] === undefined) {
        alert(id);
        console.log(id)
        const ad = await UserApi.GetAdById(id);
        if (ad.data !== null) {
          state.ads[id.toString()] = ad.data
        }
      } else{
        state.dataReady = true;
      }
    },
  },
  modules: {},
});
