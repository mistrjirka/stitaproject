import Axios, { AxiosError } from "axios";
import store from "../store";

import {
  ServerLoginResponse,
  ServerRegisterResponse,
  ServerAddAd,
  ServerGetAllAds,
  IAd,
  IAdC,
  ServerGetAd,
  ServerGetAllCategoriesResponse,
} from "../Definitionsfornetwork";
export abstract class UserApi {
  private static usersAxios = Axios.create();
  static async getLogin(
    username: string,
    password: string,
    url = "http://" + location.hostname + ":5000/login"
  ): Promise<ServerLoginResponse> {
    let responseData: string | undefined = undefined;
    let error: AxiosError | null = null;
    await this.usersAxios
      .get<string>(url, {
        params: { name: username, password: password },
      })
      .then((response) => {
        responseData = response.data;
      })
      .catch((Perror) => {
        error = Perror;
      });

    if (typeof responseData !== "undefined")
      return {
        error: null,
        data: responseData,
      };
    else return { error: error, data: null };
  }
  static async getRegister(
    username: string,
    email: string,
    password: string,
    url = "http://" + location.hostname + ":5000/register"
  ): Promise<ServerRegisterResponse> {
    let responseData: string | undefined = undefined;
    let error: AxiosError | null = null;
    await this.usersAxios
      .get<string>(url, {
        params: { name: username, email: email, password: password },
      })
      .then((response) => {
        responseData = response.data;
      })
      .catch((Perror) => {
        error = Perror;
      });

    if (typeof responseData !== "undefined")
      return {
        error: null,
        data: responseData,
      };
    else return { error: error, data: null };
  }
  static async getAllCategories(
    url = "http://" + location.hostname + ":5000/getallcategories"
  ): Promise<ServerGetAllCategoriesResponse> {
    let responseData: (
      | "Fyzika"
      | "Chemie"
      | "Český jazyk"
      | "Chemicko-biologická praktika"
      | "Dějepis"
      | "Fyzikálně-matematická praktika"
      | "Anglický jazyk"
      | "Geologie"
      | "Životní prostředí"
      | "Hudební výchova"
      | "Informatika"
      | "Matematika"
      | "Občanská výchova"
      | "Tělesná výchova"
      | "Výtvarná výchova"
      | "Základy společenských věk"
      | "Zeměpis"
      | "Přírodopis"
      | "Ostatní"
    )[] = [];
    let error: AxiosError | null = null;
    await this.usersAxios
      .get<
        (
          | "Fyzika"
          | "Chemie"
          | "Český jazyk"
          | "Chemicko-biologická praktika"
          | "Dějepis"
          | "Fyzikálně-matematická praktika"
          | "Anglický jazyk"
          | "Geologie"
          | "Životní prostředí"
          | "Hudební výchova"
          | "Informatika"
          | "Matematika"
          | "Občanská výchova"
          | "Tělesná výchova"
          | "Výtvarná výchova"
          | "Základy společenských věk"
          | "Zeměpis"
          | "Přírodopis"
          | "Ostatní"
        )[]
      >(url)
      .then((response) => {
        if (response.data !== undefined) {
          responseData = response.data;
        }
      })
      .catch((Perror) => {
        error = Perror;
      });

    if (typeof responseData !== "undefined")
      return {
        error: null,
        data: responseData,
      };
    else return { error: error, data: null };
  }
  static async getCreateAd(
    name: string,
    state: string,
    price: number,
    priceType: string,
    description: string,
    images: { data: Blob; name: string }[],
    subject: string,
    school: string,
    grade: string,
    locations: string,
    url = "http://" + location.hostname + ":5000/addad"
  ): Promise<ServerAddAd> {
    let responseData: string | undefined = undefined;
    let error: AxiosError | null = null;
    console.log("test");
    console.log(images.length);
    console.log(JSON.stringify(images));
    const form = new FormData();
    const photos: Blob[] = [];
    form.append("name", name);
    form.append("price", price.toString());
    form.append("priceType", priceType);
    form.append("description", description);
    form.append("state", state);
    form.append("token", store.state.token);
    form.append("subject", subject);
    form.append("school", school);
    form.append("grade", grade);
    form.append("location", locations);

    console.log(images);
    alert(images.length);
    images.forEach((image, index) => {
      form.append("file" + index, image.data, image.name);
    });

    await this.usersAxios
      .post<string>(
        url,
        form,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        } /*{
        params: {
          token: store.state.token,
          name: name,
          price: price,
          priceType: priceType,
          description: description,
          photos: JSON.stringify(images),
          state: state,
          subject: subject,
          school: school,
          grade: grade,
          location: location,
        },
      }*/
      )
      .then((response) => {
        responseData = response.data;
      })
      .catch((Perror) => {
        error = Perror;
      });

    if (typeof responseData !== "undefined")
      return {
        error: null,
        data: responseData,
      };
    else return { error: error, data: null };
  }

  static async GetAdsBySubject(
    subject:
      | "Fyzika"
      | "Chemie"
      | "Český jazyk"
      | "Chemicko-biologická praktika"
      | "Dějepis"
      | "Fyzikálně-matematická praktika"
      | "Anglický jazyk"
      | "Geologie"
      | "Životní prostředí"
      | "Hudební výchova"
      | "Informatika"
      | "Matematika"
      | "Občanská výchova"
      | "Tělesná výchova"
      | "Výtvarná výchova"
      | "Základy společenských věk"
      | "Zeměpis"
      | "Přírodopis"
      | "Ostatní",
    url = "http://" + location.hostname + ":5000/getadsbysubject"
  ): Promise<ServerGetAllAds> {
    let responseData: IAdC[] | undefined = undefined;
    let error: AxiosError | null = null;
    await this.usersAxios
      .get<IAd[]>(url, {
        params: {
          subject: subject,
        },
      })
      .then((response) => {
        const resData: IAdC[] = [];

        if (response.data !== null) {
          response.data!.forEach((ad, index) => {
            console.log(ad);

            resData[index] = {} as IAdC;
            resData[index].id = ad.id;
            resData[index].name = ad.name;
            resData[index].price = ad.price;

            resData[index].location = ad.location;
            resData[index].grade = ad.grade;
            resData[index].school = ad.school;
            resData[index].photos = ad.photos;
            resData[index].description = ad.description;
            resData[index].nameOfSeller = ad.nameOfSeller;
            resData[index].state = ad.state;
            resData[index].subject = ad.subject;
            store.state.ads[ad.id] = resData[index];
            console.log(resData);
          });
          responseData = resData;
        }
      })
      .catch((Perror) => {
        error = Perror;
      });

    if (typeof responseData !== "undefined")
      return {
        error: null,
        data: responseData,
      };
    else return { error: error, data: null };
  }

  static async GetAdById(
    id: number,
    url = "http://" + location.hostname + ":5000/getadbyid"
  ): Promise<ServerGetAd> {
    alert(id);
    let responseData: IAdC | undefined = undefined;
    let error: AxiosError | null = null;
    await this.usersAxios
      .get<IAd>(url, {
        params: {
          id: id,
        },
      })
      .then((response) => {
        if (response.data !== null) responseData = response.data;
      })
      .catch((Perror) => {
        error = Perror;
      });

    if (typeof responseData !== "undefined")
      return {
        error: null,
        data: responseData,
      };
    else return { error: error, data: null };
  }
}
