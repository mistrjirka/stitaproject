import { IAd } from "../Definitionsfornetwork"

export interface IUser {
  id: number;
  email: string;
  create_time: string;
  messages: string;
  username: string;
  orders: string;
}

export interface ILoginForm {
  name: string;
  password: string;
}
export interface IRegisterForm {
  name: string;
  password: string;
  email: string;
}

export interface ILoginResponse {
  token: string;
}
export interface ICachedAds {
  [key: string]: IAd;
}