import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import LoginPage from "../components/loginPage.vue";
import RegisterPage from "../components/registerPage.vue";
import Login from "../views/Login.vue";
import store from "../store";
import Loggedin from "../views/Loggedin.vue";
import SignPost from "../components/signpost.vue";
import CreateAd from "../views/CreateAd.vue";
import IndexPage from "../views/Index.vue";
import LoggedinCategory from "../views/LoggedinCategory.vue";
import AdDetail from "../views/AdDetail.vue";
Vue.use(VueRouter);
function lazyLoad(view: string) {
  return () => import(`../views/${view}.vue`);
}
export const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: IndexPage,
    children: [
      {
        path: "/login",
        components: { default: LoginPage },
      },
      {
        path: "/register",
        components: { default: RegisterPage },
      },
      {
        path: "/signpost",
        components: { default: SignPost },
      },
      {
        path: "/",
        name: "Rozcestník",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        components: {
          default: SignPost,
        },
      },
      {
        path: "/category/:category",
        name: "Inzeráty",
        component: LoggedinCategory,

        props: true,
      },
      {
        path: "/ad/:id",
        name: "Inzeráty",
        component: AdDetail,
        props: true,
      },
      {
        path: "/createad",
        name: "Vytvoření zakázky",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        components: {
          default: CreateAd,
        },
      },
      {
        path: "/main",
        name: "main",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        components: {
          default: Loggedin,
        },
      },
    ],
  },
];

const router = new VueRouter({
  routes,
});

export default router;
