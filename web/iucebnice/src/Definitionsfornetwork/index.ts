import { AxiosError } from "axios";
import { IUser, ILoginResponse } from "../Definitions";
export interface ServerLoginResponse {
  data: ILoginResponse | null;
  error: AxiosError | null;
}

export interface ServerRegisterResponse {
  data: string | null;
  error: AxiosError | null;
}
export interface ServerGetAllUserInfoResponse {
  data: IUser | null;
  error: AxiosError | null;
}
export interface ServerAddAd {
  data: string | null;
  error: AxiosError | null;
}
export interface ServerGetCategories {
  data: (
    | "Fyzika"
    | "Chemie"
    | "Český jazyk"
    | "Chemicko-biologická praktika"
    | "Dějepis"
    | "Fyzikálně-matematická praktika"
    | "Anglický jazyk"
    | "Geologie"
    | "Životní prostředí"
    | "Hudební výchova"
    | "Informatika"
    | "Matematika"
    | "Občanská výchova"
    | "Tělesná výchova"
    | "Výtvarná výchova"
    | "Základy společenských věk"
    | "Zeměpis"
    | "Přírodopis"
    | "Ostatní"
  )[] | null;
  error: AxiosError | null;
}
export interface IAd {
  id: number;
  name: string;
  price: {
    type: "disscutable" | "static";
    price: number;
  };
  photos: string[];
  create_time: string;
  description: string;
  nameOfSeller: string;
  state: string;
  subject:
    | "Fyzika"
    | "Chemie"
    | "Český jazyk"
    | "Chemicko-biologická praktika"
    | "Dějepis"
    | "Fyzikálně-matematická praktika"
    | "Anglický jazyk"
    | "Geologie"
    | "Životní prostředí"
    | "Hudební výchova"
    | "Informatika"
    | "Matematika"
    | "Občanská výchova"
    | "Tělesná výchova"
    | "Výtvarná výchova"
    | "Základy společenských věk"
    | "Zeměpis"
    | "Přírodopis"
    | "Ostatní"; //Biologie, Fyzika, Chemie, Český jazyk, Chemicko-biologická praktika, Chemie, Dějepis, Fyzikálně-matematická praktika, Anglický jazyk, Geologie, Geologie a životní prostředí, Hudební Výchova, Informatika, Matematika, Občanská výchova, Tělesná výchova, Výtvarná Výchova, Základy společenských věk, Zdravá člověka, Zeměpis, Přírodopis, Ostatní
  grade: number;
  location: string;
  school: "Vysoká škola" | "Střední Škola";
}
export interface IAdC {
  id: number;
  name: string;
  price: {
    type: "disscutable" | "static";
    price: number;
  };
  photos: string[];
  create_time: string;
  description: string;
  nameOfSeller: string;
  state: string;
  subject:
    | "Fyzika"
    | "Chemie"
    | "Český jazyk"
    | "Chemicko-biologická praktika"
    | "Dějepis"
    | "Fyzikálně-matematická praktika"
    | "Anglický jazyk"
    | "Geologie"
    | "Životní prostředí"
    | "Hudební výchova"
    | "Informatika"
    | "Matematika"
    | "Občanská výchova"
    | "Tělesná výchova"
    | "Výtvarná výchova"
    | "Základy společenských věk"
    | "Zeměpis"
    | "Přírodopis"
    | "Ostatní"; //Biologie, Fyzika, Chemie, Český jazyk, Chemicko-biologická praktika, Chemie, Dějepis, Fyzikálně-matematická praktika, Anglický jazyk, Geologie, Geologie a životní prostředí, Hudební Výchova, Informatika, Matematika, Občanská výchova, Tělesná výchova, Výtvarná Výchova, Základy společenských věk, Zdravá člověka, Zeměpis, Přírodopis, Ostatní
  grade: number;
  location: string;
  school: "Vysoká škola" | "Střední Škola";
}
export interface ServerGetAllAds {
  data: IAd[] | null;
  error: AxiosError | null;
}

export interface ServerGetAd {
  data: IAd | null;
  error: AxiosError | null;
}

export interface RemoveAd {
  data: string | null;
  error: AxiosError | null;
}

export interface ServerGetAllCategoriesResponse {
  data:
    | (
        | "Fyzika"
        | "Chemie"
        | "Český jazyk"
        | "Chemicko-biologická praktika"
        | "Dějepis"
        | "Fyzikálně-matematická praktika"
        | "Anglický jazyk"
        | "Geologie"
        | "Životní prostředí"
        | "Hudební výchova"
        | "Informatika"
        | "Matematika"
        | "Občanská výchova"
        | "Tělesná výchova"
        | "Výtvarná výchova"
        | "Základy společenských věk"
        | "Zeměpis"
        | "Přírodopis"
        | "Ostatní"
      )[]
    | null;
  error: AxiosError | null;
}
