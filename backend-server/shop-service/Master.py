class Master():
    def __init__(self, name, myId, DM):
        self.DM = DM
        self.name = name
        self.id = myId
        config = self.DM.getUserData(myId)
        print(config, flush=True)
        self.messages = config["messages"]
        self.create_time = config["create_time"]
    def addAd(self, name, price, priceType, description, photos, location, school, grade, subject):
        self.DM.addAd(self.id, name, description, {"type": priceType, "cost": price}, self.name, photos, location, school, grade, subject)
    def removeAd(self, id):
        self.DM.deleteAd(id, self.id)
    def messageSomeone(self):
        pass
    def fetchInfo(self):
        pass
    def fetchMessages(self):
        pass
    
