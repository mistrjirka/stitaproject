from flask import Flask, send_file
from flask_restful import Api, Resource
from flask_api import status
from datetime import datetime
from flask_debugtoolbar import DebugToolbarExtension
import mysql.connector
from flask_cors import CORS
import json
from Dataman import DatabaseManipulation
from Tokens import TokenCreator 
import lzstring
import threading
from PIL import Image
import base64
import os.path
from os import path
#from Tokens import CronDeamon
import re
import io
import definitions as Parser
def check(email):  
    regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
    # pass the regualar expression 
    # and the string in search() method 
    return(re.search(regex,email))
class Studit:
    def __init__(self, name, port=800,debug=True):
        
        self.name=name
        print("sig", flush=False)
        print(port, flush=False)
        print(debug, flush=False)
        print(port)
        print(debug)
        app = Flask(__name__)  # creating app
        api = Api(app)  # creating api
        toolbar = DebugToolbarExtension(app)
        TC = TokenCreator(app)
        DM = DatabaseManipulation(app, self.name)
           
        categories = []
        class CBasic(Resource):
            def get(self):
                return "OK", status.HTTP_200_OK
        class CRegister(Resource):
            def get(self):
                args = Parser.Register.parse_args()
                name = args["name"]
                password = args["password"]
                email = args["email"]
                validated = check(email)
                if not validated:
                    return "bad email", status.HTTP_401_UNAUTHORIZED
                if len(name) > 20:
                    return "too long username", status.HTTP_401_UNAUTHORIZED
                if len(password) < 6:
                    return "too short password please use password longer than 6 characters", status.HTTP_401_UNAUTHORIZED
                if DM.register(name, password, email):
                    return "OK", status.HTTP_200_OK
                else:
                    print("error")
                    return "You are already registred", status.HTTP_500_INTERNAL_SERVER_ERROR

        class CRegisterMaster(Resource):
            def get(self):
                args = Parser.RegisterMaster.parse_args()
                name = args["name"]
                password = args["password"]
                email = args["email"]
                creationToken = args("creation_token")
                validated = check(email)
                
                if not validated:
                    return "bad email", status.HTTP_401_UNAUTHORIZED
                if len(name) > 20:
                    return "too long username", status.HTTP_401_UNAUTHORIZED
                if len(password) < 6:
                    return "too short password please use password longer than 6 characters", status.HTTP_401_UNAUTHORIZED
                if DM.registerMaster(name, password, email):
                    return "OK", status.HTTP_200_OK
                else:
                    print("error")
                    return "You are already registred or bad code", status.HTTP_500_INTERNAL_SERVER_ERROR

        class CLogin(Resource):
            def get(self):
                args = Parser.Login.parse_args()  # compiling args to object

                name = args["name"]
                password = args["password"]

                logged = DM.verifyLogin(name, password)
                if logged["logged"]:
                    token = TC.createUniqeToken(logged["id"], 84000, name, DM)
                    return token, status.HTTP_202_ACCEPTED
                else:
                    return logged["status"], status.HTTP_401_UNAUTHORIZED

            def options(self):
                pass
                
        class CGetAllCategories(Resource):
            def get(self):
                return DM.getAllSubjects(), status.HTTP_200_OK

        class CAddAd(Resource): # potencionálně nebezpečné, předělat na systém, kde se budou jednotlivě přidávat parametry
            def post(self):
                print("ahoj")
                args = Parser.addAd.parse_args()
                token = args["token"]
                name = args["name"]
                priceType = args["priceType"]
                price = args["price"]
                description = args["state"]
                files=[
                    args["file0"],
                    args["file1"],
                    args["file2"],
                    args["file3"],
                    args["file4"]
                ]
                subject = args["subject"]
                grade = args["grade"]
                location = args["location"]
                school = args["school"]
                correctPhotos = True
                photosDumpedDecompressed = []
                for index, file0 in enumerate(files):
                    works = False
                    try:
                        a = file0.read()
                        works = True
                    except Exception as e:
                        print("error", flush=True)
                    
                    def stringToRGB(imgs):
                        return io.BytesIO(imgs)

                    if works or index == 0:
                        try:
                            #photosDumped = json.loads(photos)
                        
                            """photoDecompressed = x.decompressFromBase64(photo)
                            
                            imgData = base64.b64decode(photoDecompressed              
                            imgData = base64.b64decode(photo+"=")
                            print(imgData, flush=True)
                            image_stream = io.BytesIO()
                            image_stream.write(imgData)
                            image_stream.seek(0)"""

                            im = Image.open(file0)
                            width, height = im.size
                            print(width, flush=True)
                            print(height, flush=True)
                            sizeNotOk = width < 200 and height < 100 
                            photosDumpedDecompressed.append({"image": im, "format": im.format})
                            if sizeNotOk or type(width) is not int:
                                correctPhotos = False
                            

                        except Exception as e:
                            print("fuck")
                            print(e, flush=True)
                            correctPhotos = False            
                print(correctPhotos)
                correctPriceNumber = True
                try:
                    price = int(price)
                except:
                    correctPriceNumber = False

                user = TC.getUserByToken(token)
                correctPriceType = priceType == "disscutable" or priceType == "static"
                if correctPhotos and user and correctPriceType and correctPriceNumber:
                    user.addAd(name, price, priceType, description, photosDumpedDecompressed, location, school, grade, subject)
                    return "OK" + location +"done", status.HTTP_200_OK
                else:
                    return "Not in session OR image error", status.HTTP_401_UNAUTHORIZED
        class CDeleteAd(Resource):
            def get(self):
                args = Parser.removeAd.parse_args()
                token = args["token"]
                idOfAd = args["id"]
                user = TC.getUserByToken(token)
                if user:
                    user.removeAd(idOfAd)
                    return "OK", status.HTTP_200_OK
                else:
                    return "Not in session", status.HTTP_401_UNAUTHORIZED

        class CGetAdsByCategory(Resource):
            def get(self):
                args = Parser.getBySubject.parse_args()
                subject = args["subject"]
                
                return DM.getAdsBySubject(subject), status.HTTP_200_OK

        class CGetAdById(Resource):
            def get(self):
                args = Parser.getById.parse_args()
                adId = args["id"]
                ad = DM.getAdsById(adId)
                if ad:
                    return ad, status.HTTP_200_OK
                else:
                    return "not found", status.HTTP_404_NOT_FOUND
        class CGetFiles(Resource):
            def get(self,OrderId, name):
                return send_file("/tmp/"+OrderId+"/"+name)
        
        api.add_resource(CBasic, "/")
        api.add_resource(CRegister, "/register")
        api.add_resource(CLogin, "/login")
        api.add_resource(CAddAd, "/addad")
        api.add_resource(CDeleteAd, "/deletead")
        api.add_resource(CGetAllCategories, "/getallcategories")
        api.add_resource(CGetAdsByCategory, "/getadsbysubject")
        api.add_resource(CGetFiles, "/images/<string:OrderId>/<string:name>")
        api.add_resource(CGetAdById, "/getadbyid")
        cors = CORS(app, host="0.0.0.0", debug=debug, port=port, threaded=True)
        app.run(host="0.0.0.0", debug=debug, port=port, threaded=True)


deamonsDen = []


deamon = threading.Thread(target=Studit, args=("global",800, False,))
deamon2 = threading.Thread(target=Studit, args=("gymvla",801, False,))

deamon.start()
deamon2.start()
