from flask_restful import reqparse
import werkzeug

Login = reqparse.RequestParser()  # tool for analyzing parameters
# defining parameter name
Login.add_argument('name', type=str, required=True)
# defing parameter password
Login.add_argument('password', type=str, required=True)


Register = reqparse.RequestParser()  # tool for analyzing parameters
# defining parameter name
Register.add_argument('name', type=str, required=True)
# defing parameter password
Register.add_argument('password', type=str, required=True)
# defing parameter email
Register.add_argument('email', type=str, required=True)


RegisterMaster = reqparse.RequestParser()  # tool for analyzing parameters
# defining parameter name
RegisterMaster.add_argument('name', type=str, required=True)
# defing parameter password
RegisterMaster.add_argument('password', type=str, required=True)
# defing parameter email
RegisterMaster.add_argument('email', type=str, required=True)

RegisterMaster.add_argument('creation_token', type=str, required=True)


addAd = reqparse.RequestParser()
addAd.add_argument("name", type=str, required=True)
addAd.add_argument("token", type=str, required=True)
addAd.add_argument("priceType", type=str, required=True)
addAd.add_argument("price", type=int, required=True)
addAd.add_argument("description", type=str, required=True)
addAd.add_argument("state", type=str, required=True)
addAd.add_argument("file0", type=werkzeug.datastructures.FileStorage, location='files', required=True)
addAd.add_argument("file1", type=werkzeug.datastructures.FileStorage, location='files', required=False)
addAd.add_argument("file2", type=werkzeug.datastructures.FileStorage, location='files', required=False)
addAd.add_argument("file3", type=werkzeug.datastructures.FileStorage, location='files', required=False)
addAd.add_argument("file4", type=werkzeug.datastructures.FileStorage, location='files', required=False)
addAd.add_argument("school", type=str, required=True)
addAd.add_argument("grade", type=str, required=True)
addAd.add_argument("subject", type=str, required=True)
addAd.add_argument("location", type=str, required=True)


removeAd = reqparse.RequestParser()
removeAd.add_argument("token", type=str, required=True)
removeAd.add_argument("id", type=int, required=True)


getBySubject = reqparse.RequestParser()
getBySubject.add_argument("subject", type=str, required=True)


getById = reqparse.RequestParser()
getById.add_argument("id", type=str, required=True)

getImages = reqparse.RequestParser()
getImages.add_argument("name", type=str, required=True)
getImages.add_argument("id", type=str, required=True)