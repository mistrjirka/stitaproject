import random
from datetime import datetime, timedelta
import hashlib
import binascii
import os
import mysql.connector
import json
from copy import deepcopy
import numpy as np
import os
import io
from PIL import Image

templateMessage = {"time": "", "content": ""}
templateMessages = {
    "me": [],
    "stranger":[]
}
templateOrder = {
    "name": "something",
    "valid_thru": "something",
    "description": "something",
    "price": {"type": "disscutable", "price": 200}, #{"type": "static", "price": 200},
    "nameOfSeller": "name",
    "state": "sold", # selling, booked
    "subject": "Biologie",
    "photos": ["BASE64IMAGEDELTA"],
    "id": 45648,
    "location": "Prague",
    "grade": "3. třída",
    "school": "ZŠ Bystřice"
    }
templateOrders = []
class DatabaseManipulation():
    def __init__(self, app, name):
        self.name = name
        self.userDatabase = mysql.connector.connect(  # connection to our main database
            host="mysql-service-dan",
            user="root",
            passwd="example",
            port=3306,
            database="users"
        )
        self.cursor = self.userDatabase.cursor(buffered=True)
        self.cursor.execute(
            """CREATE TABLE IF NOT EXISTS `users`.`user` (
            `id` INT NOT NULL,
            `username` VARCHAR(16) NOT NULL,
            `email` VARCHAR(255) NULL,
            `password` VARCHAR(255) NOT NULL,
            `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
            `messages` JSON NULL,
            `kosik` JSON NULL,
            `access` VARCHAR(45) NULL,
            PRIMARY KEY (`id`));"""
        )  
        self.cursor.execute(
            """CREATE TABLE IF NOT EXISTS `users`.`OrdersTable` (
            `id` INT NULL,
            `orderid` INT NULL,
            `name` VARCHAR(45) NULL,
            `create_time` VARCHAR(45) NULL,
            `description` MEDIUMTEXT NULL,
            `pricetype` VARCHAR(45) NULL,
            `cost` VARCHAR(45) NULL,
            `state` VARCHAR(45) NULL,
            `subject` VARCHAR(45) NULL,
            `location` VARCHAR(45) NULL,
            `grade` VARCHAR(45) NULL,
            `school` VARCHAR(45) NULL,
            `access` VARCHAR(45) NULL,
            INDEX `fk_OrdersTable_1_idx` (`id` ASC) VISIBLE,
            CONSTRAINT `fk_OrdersTable_1`
                FOREIGN KEY (`id`)
                REFERENCES `users`.`user` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)"""
        )
        self.cursor.execute(
            """CREATE TABLE IF NOT EXISTS `users`.`MasterRegistration` (
            `id` INT NOT NULL,
            `access` VARCHAR(45) NULL,
            `name` VARCHAR(45) NULL,
            `code` VARCHAR(55) NULL,
            PRIMARY KEY (`id`))
            ENGINE = InnoDB;"""
        )
        self.cursor.execute(
            """CREATE TABLE IF NOT EXISTS `users`.`master` (
            `master` INT NOT NULL,
            `username` VARCHAR(16) NOT NULL,
            `email` VARCHAR(255) NULL,
            `password` VARCHAR(255) NOT NULL,
            `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
            `messages` JSON NULL,
            `kosik` JSON NULL,
            `access` VARCHAR(45) NULL,
            PRIMARY KEY (`master`));"""
        )
        self.app = app
        self.cursor.execute("CREATE or replace VIEW ViewOrders" + self.name+" AS SELECT * FROM OrdersTable Where access=%s",(self.name,))
        self.userDatabase.commit()
        self.cursor.execute("CREATE or replace VIEW ViewUser"+ self.name + " AS SELECT * FROM user Where access=%s", (self.name,))
        self.userDatabase.commit()
        self.cursor.execute("CREATE or replace VIEW ViewMasterRegistration"+ self.name + " AS SELECT * FROM MasterRegistration Where access=%s", (self.name,))
        self.userDatabase.commit()
        self.cursor.execute("CREATE or replace VIEW ViewMaster"+ self.name + " AS SELECT * FROM master Where access=%s", (self.name,))
        self.userDatabase.commit()
    def generateOrderId(self):
        uniqe = False
        number = random.randint(1, 10000000)  # generates the number
        while not uniqe:  # repeats until number is uniqe
            number = random.randint(1, 10000000)
            self.cursor.execute("SELECT * FROM OrdersTable")
            result = self.cursor.fetchall()
            uniqe = True
            for i in result:
                if int(i[1]) == number:
                    uniqe = False
                    break

            
        return number

    def generateId(self):  # generates uniqe number
        uniqe = False
        number = random.randint(1, 10000000)  # generates the number
        idcurosor = self.userDatabase.cursor(buffered=True)
        while not uniqe:  # repeats until number is uniqe
            number = random.randint(1, 10000000)
            self.cursor.execute("SELECT * FROM user WHERE id = %s", (number,))  # Ask database if there is any id that is same as the generated one
            result = self.cursor.fetchall()
            uniqe = len(result) == 0  # if not repeat in yes end the loop
        self.userDatabase.commit()
        idcurosor.close()
        return number
    def generateMasterId(self):  # generates uniqe number
        uniqe = False
        number = random.randint(1, 10000000)  # generates the number
        idcurosor = self.userDatabase.cursor(buffered=True)
        while not uniqe:  # repeats until number is uniqe
            number = random.randint(1, 10000000)
            idcurosor.execute("SELECT * FROM master WHERE id = %s", (number,))  # Ask database if there is any id that is same as the generated one
            result = self.cursor.fetchall()
            uniqe = len(result) == 0  # if not repeat in yes end the loop
        return number
    def register(self, name, password, email):
        if not self.checkIfNotRegistred(name, email):
            return False
        else:
            registercurosor = self.userDatabase.cursor(buffered=True)
            today = datetime.now()
            myId = self.generateId()
            os.makedirs("/tmp/"+str(myId)+"/")
            registercurosor.execute("""INSERT INTO user(id, password, create_time, email, username, messages, access) VALUES(%s, %s, %s,%s,%s,%s,%s);""", (
                myId, self.hash_password(password), today.strftime('%Y-%m-%d %H:%M:%S'), email,name, json.dumps(templateMessages),self.name,))
            self.userDatabase.commit()
            registercurosor.close()
            return True
    
    def registerMaster(self, name, password, email, code):
        if not self.checkIfNotRegistred(name, email):
            return False
        else:
            today = datetime.now()
            myId = self.generateId()
            os.makedirs("/tmp/"+str(myId)+"/")
            self.cursor.executemany("""INSERT INTO user(id, password, create_time, email, username, messages, access) VALUES(%s, %s, %s,%s,%s,%s,%s);""", (
                myId, self.hash_password(password), today.strftime('%Y-%m-%d %H:%M:%S'), email,name, json.dumps(templateMessages),self.name,))
            self.userDatabase.commit()
            return True
            
    def checkIfNotRegistred(self, name, email):
        checkcurosor = self.userDatabase.cursor(buffered=True)
        checkcurosor.execute("""SELECT * FROM user WHERE username = %s or email = %s""",
                            (name, email,))  # prevention from sql injection
        result = self.cursor.fetchone()
        self.userDatabase.commit()
        checkcurosor.close()
        if result == None:
            return True
        else:
            return False
    def getUserData(self,id):
        self.cursor.execute("""SELECT * FROM ViewUser"""+ self.name + """
          WHERE id=%s""", (id,))
        self.userDatabase.commit()
        result = self.cursor.fetchone()
        return {
            "id": result[0],
            "name": result[1],
            "email": result[2],
            "create_time": result[4],
            "messages": result[5],
        }
    def addAd(self, idOfUser, name, description, price, nameOfSeller, photos, location, school, grade, subject):
        time = datetime.now() + timedelta(days=30)
        generated = self.generateOrderId()
        """addTemplate =  {
            "name": name,
            "valid_thru": time.strftime('%Y-%m-%d %H:%M:%S'),
            "description": description,
            "price": price, #{"type": "static", "price": 200},
            "nameOfSeller": nameOfSeller,
            "state": "selling", # selling, booked
            "id": generated,
            "location": location,
            "school": grade,
            "subject": subject,
            "grade": grade,
            "access":
        }"""
        today = datetime.now()
        self.cursor.execute("""INSERT INTO OrdersTable(id, orderid, create_time, name, description, pricetype, cost, state, subject, location, grade, school, access) VALUES(%s,%s, %s, %s,%s,%s,%s,%s,%s, %s, %s,%s,%s);""", (
                idOfUser, generated, today.strftime('%Y-%m-%d %H:%M:%S'), name, description, price["type"], price["cost"], "selling", subject, location, grade, school, self.name))
        self.userDatabase.commit()
        if not os.path.exists("/tmp/"+str(idOfUser)+"/"):
            os.makedirs("/tmp/"+str(idOfUser)+"/")
        for index, file  in enumerate(photos):
            """textfile = open("/tmp/"+str(idOfUser)+"/"+str(generated)+str(index)+'.base64compressed', 'w')
            textfile.write(file)
            textfile.close()"""
            
            name = "/tmp/"+str(idOfUser)+"/"+str(generated)+str(index)+"."+file["format"]
            print(name, flush=True)
            file["image"].save(name)
        
        
        return True
    def hash_password(self, password):
        """Hash a password for storing."""
        salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
        pwdhash = hashlib.pbkdf2_hmac(
            'sha512', password.encode('utf-8'), salt, 100000)
        pwdhash = binascii.hexlify(pwdhash)
        return (salt + pwdhash).decode('ascii')
    def getAllSubjects(self):
        self.cursor.execute("SELECT subject FROM ViewOrders"+ self.name + " WHERE 1=1 ")
        result = self.cursor.fetchall()
        array = []
        for ad in result:
            if ad[0] not in array:
                array.append(ad[0])
        return array
    

    def getAllAds(self):
        self.cursor.execute("SELECT * FROM ViewOrders"+ self.name + " WHERE 1=1 ")
        result = self.cursor.fetchall()
        array = []
        for Ad in result:
            photos=[]
            #try:
            for filename in os.listdir("/tmp/"+str(Ad[0])+"/"):
                if filename.endswith(".base64compressed") and str(Ad[1]) in filename:
                    f = open("/tmp/"+str(Ad[0])+"/"+filename, "r")
                    photos.append(f.read())
            #except:
            #   print("file does not exist")
            array.append({
                "id": Ad[1],
                "name": Ad[2],
                "create_time": Ad[3],
                "description": Ad[4],
                "price": {
                    "type": Ad[5],
                    "price": Ad[6]
                },
                "photos": photos,
                "state": Ad[7],
                "subject": Ad[8],
                "location": Ad[9],
                "grade": Ad[10],
                "school": Ad[11]

            })            
        return array
    def deleteAd(self, IdOfAd, id):
        ads = deepcopy(self.getAdsOfUser(id))
        editedAds = deepcopy(ads)
        index = 0;  
        for ad in ads:
            if ad["id"] == IdOfAd:
                del editedAds[index]
                self.updateUserAds(id, editedAds)
                return True
            index += 1
        return False
    def updateUserAds(self, id, ads):
        compiledads = json.dumps(ads)
        self.cursor.execute("""UPDATE OrdersTable SET orders = %s WHERE id=%s;""", (compiledads, id,))
        self.userDatabase.commit()
        return True
    def getAdsOfUser(self, id):
        self.cursor.execute("""SELECT * FROM ViewOrders"""+ self.name + """ WHERE id=%s """, (id))
        self.userDatabase.commit()
        result = self.cursor.fetchall()
        array = []
        for Ad in result:
            photos=[]
            #try:
            for filename in os.listdir("/tmp/"+str(Ad[0])+"/"):
                
                if str(Ad[1]) in filename:
                    photos.append(filename)
            #except:
            #   print("file does not exist")
            array.append({
                "id": Ad[1],
                "name": Ad[2],
                "create_time": Ad[3],
                "description": Ad[4],
                "price": {
                    "type": Ad[5],
                    "price": Ad[6]
                },
                "photos": photos,
                "state": Ad[7],
                "subject": Ad[8],
                "location": Ad[9],
                "grade": Ad[10],
                "school": Ad[11]

            })            
        print("hello")
        print(array, flush=True)
        print("hello")  
        return array
    def getAdsBySubject(self, subject):
        print("SELECT * FROM ViewOrders"+ self.name + " WHERE subject=%s ", flush=False)
        self.cursor.execute("SELECT * FROM ViewOrders"+ self.name + " WHERE subject=%s ", (subject,))
        result = self.cursor.fetchall()
        array = []
        for Ad in result:
            photos=[]
            #try:
            for filename in os.listdir("/tmp/"+str(Ad[0])+"/"):
                if str(Ad[1]) in filename:
                    photos.append("images/"+str(Ad[0])+"/"+filename)
            #except:
            #   print("file does not exist")
            array.append({
                "id": Ad[1],
                "name": Ad[2],
                "create_time": Ad[3],
                "description": Ad[4],
                "price": {
                    "type": Ad[5],
                    "price": Ad[6]
                },
                "photos": photos,
                "state": Ad[7],
                "subject": Ad[8],
                "location": Ad[9],
                "grade": Ad[10],
                "school": Ad[11]

            })
        return array 
            

    def getAdsById(self, AdId):
        self.cursor.execute("""SELECT * FROM ViewOrders"""+ self.name + """ WHERE id=%s """, (AdId,))
        result = self.cursor.fetchall()
        ad = {}
        Ad = result[0]
        photos=[]
        if len(result == 0):
            return False
        #try:
        for filename in os.listdir("/tmp/"+str(Ad[0])+"/"):
            if str(Ad[1]) in filename:
                photos.append("images/"+str(Ad[0])+"/"+filename)
        #except:
        #   print("file does not exist")
        Ad.append({
            "id": Ad[1],
            "name": Ad[2],
            "create_time": Ad[3],
            "description": Ad[4],
            "price": {
                "type": Ad[5],
                "price": Ad[6]
            },
            "photos": photos,
            "state": Ad[7],
            "subject": Ad[8],
            "location": Ad[9],
            "grade": Ad[10],
            "school": Ad[11]

        })
        return array 
            

    def verify_password(self, stored_password, provided_password):
        """Verify a stored password against one provided by user"""
        salt = stored_password[:64]
        stored_password = stored_password[64:]
        pwdhash = hashlib.pbkdf2_hmac('sha512',
                                      provided_password.encode('utf-8'),
                                      salt.encode('ascii'),
                                      100000)
        pwdhash = binascii.hexlify(pwdhash).decode('ascii')
        return pwdhash == stored_password
    def verifyLogin(self, name, password, email=None):
        self.cursor.execute("""SELECT * FROM ViewUser"""+ self.name + """ WHERE username = %s""",
                            (name,))  # prevention from sql injection
        result = self.cursor.fetchall()
        passwordTest = False
        if(len(result) != 0):
            passwordTest = self.verify_password(result[0][3], password)
        if email == None:
            if len(result) == 0:
                return {
                    "logged": False,
                    "status": "user not found",
                    "id": None
                }
            elif passwordTest:

                return {
                    "logged": passwordTest,
                    "status": "Logged in ",
                    "id": result[0][0],
                }
            else:
                return {
                    "logged": passwordTest,
                    "status": "wrong password",
                    "id": result[0][0],
                }
        else:
            if len(result) == 0 or result[0][4] != email:
                return {
                    "logged": False,
                    "status": "user not found",
                    "id": None
                }
            elif passwordTest:

                return {
                    "logged": passwordTest,
                    "status": "Logged in",
                    "id": result[0][0],
                }
            else:
                return {
                    "logged": passwordTest,
                    "status": "wrong password",
                    "id": result[0][0]
                }