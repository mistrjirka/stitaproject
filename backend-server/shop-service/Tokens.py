import threading
from time import sleep
import string
import random
import sys
from User import User
import copy 
sessions = []
users = []# {"userClass": Class, "id": number}

def randomString(stringLength=30):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase + string.digits
    return ''.join(random.choice(letters) for i in range(stringLength))


class TokenDeamon():
    def __init__(self):
        self.stop = False
        self.deamon = threading.Thread(target=self.deamonFun, args=())
        self.deamon.start()

    def deamonFun(self):
        while self.stop is not True:
            for index, user in enumerate(sessions):
                user["time"] = user["time"] - 10
                if user["time"] <= 0:
                    del sessions[index]
            sleep(10)

    def stopDeamon(self):
        self.stop = True

"""class CronDeamon(function, time):
    self.stop = False
    self.deamon = threading.Thread(target=self.deamonFun, args=(function, time))
    self.deamon.start()
    def deamonFun(self, function, time):
        while self.stop is not True:
            function()
            sleep(time)

    def stopDeamon(self):
        self.stop = True
"""
class TokenCreator():  # creates session token
    def __init__(self, app):
        app.logger.info("test2")
        self.app = app
        self.TD = TokenDeamon()
 

    def tokenVerify(self, token):         
        for i in sessions: 
            if(i["token"] == token):
                return True
        return False
        
    def createUniqeToken(self, myId, time,  name, DM):  # time in seconds
        token = randomString()
        while self.tokenVerify(token):
            token = randomString()

        userlogged = False
        for i in users:
            if i["id"] == myId:
                userlogged = True
        
        if not userlogged:    
            users.append({"id": myId, "userClass": User(name, myId, DM)})
        sessions.append({"id": myId, "time": time, "token": token})
        print(self.tokenVerify(token), flush=True)
        return token

    def destroyToken(self, token):
        for index, user in enumerate(sessions):
            if user["token"] == token:
                myId = copy.copy(user)["id"]
                del sessions[index]
                for i in sessions:
                    if i["id"] == myId:
                        return True
                
                for indexJ, j in enumerate(users):
                    if j["id"] == myId:
                        del users[indexJ]
                
                return True

        return False
    def getIdByToken(self, token):
        for user in sessions:
            if user["token"] == token:
                return user["id"]
        return False
    def getUserByToken(self, token):
        for session in sessions:
            if session["token"] == token:               
                for user in users:
                    if user["id"] == session["id"]:
                        return user["userClass"]
                
        return False

